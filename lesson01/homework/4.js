let b = 0;

alert('Please, enter TRUE or FALSE');

for (let i = 0; i < 6; i++) {
    let bool = confirm('If it is true, click OK; if it is false, CANCEL');
    if (bool) b++;
    alert('You have just entered ' + bool);
};

alert(`Number of TRUE answers is ${b}`);