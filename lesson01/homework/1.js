/* eslint-disable */

// const 1a = 1 
// let b-b = "тут вроде строка"
// const Булин = false 
// let %zero% =  null
// const забытая(переменная) = undefined
// let switch = {} 
// function variable = new Function()


const a = 1;
let b = "тут вроде строка";
const Bulin = false;
let zero = null;
const forgottenVariable = undefined;
mySwitch = {};
variable = new Function();

alert('Variable a is of ' + typeof(a) + ' type');
alert('Variable b is of ' + typeof(b) + ' type');
alert('Variable Bulin is of ' + typeof(Bulin) + ' type');
alert('Variable zero is of ' + typeof(zero) + ' type');
alert('Variable forgottenVariable is of ' + typeof(forgottenVariable) + ' type');
alert('Variable mySwitch is of ' + typeof(mySwitch) + ' type');
alert('Variable variable is of ' + typeof(variable) + ' type');