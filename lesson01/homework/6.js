// -Подключить файл 6.js.
// -Получить из данной в файле строки(result) число 11, 5, -5, 0
// -Нельзя удалять / менять операторы или операнды
// -Нельзя менять порядок
// -Можно и нужно преобразовать(там где необходимо) типы данных при присвоении
// -Обязательно использовать инкремент или декремент в префиксной
// или постфиксной форме для каждого операнда

/* eslint-disable */



// let a = Number(!Boolean('ES6')); //0
// let b = Number(Boolean("false")); //1
// let c = Number(Boolean(NaN)); //0
// let d = Number(true); //1
// let e = Number(Boolean(undefined)); //0
// let f = Number('0'); //0
// let g = Number(null); //0
// let h = 0;

// console.log(result);
//Works for 3 results below:

// let result = ++a + ++b + ++c + d + e + f + g + h; //result = 5
// let result = --a + --b + --c + --d + --e + --f + --g + h; //result = -5
//let result = a + b + c + d + e + --f + --g + h; //result = 0


let a = Number(Boolean('ES6')); //0
let b = Number(Boolean("false")); //1
let c = Number(Boolean(NaN)); //0
let d = Number(true); //1
let e = Number(Boolean(undefined)); //0
let f = Number('0'); //0
let g = Number(null); //0
let h = 0;

let result = ++a + ++b + ++c + ++d + ++e + ++f + ++g + ++h; //result = 11

console.log(result);